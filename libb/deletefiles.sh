#!/bin/bash

files_to_delete="libc.so.6
libdl.so.2
libgcc_s.so.1
libGLdispatch.so.0
libglib-2.0.so.0
libGL.so.1
libGLX.so.0
libgthread-2.0.so.0
libICE.so.6
libm.so.6
libpcre.so.3
libpthread.so.0
libSM.so.6
libstdc++.so.6
libuuid.so.1
libX11.so.6
libXau.so.6
libxcb.so.1
libXdmcp.so.6
libXext.so.6
libXt.so.6
libz.so.1"


for file in $files_to_delete
do
	rm $file
done


